#!/usr/bin/env bash

DEFAULT_KEY="ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJ59XMELXMBalvkM505nCukcJv1jOag3RvUysBzkxHWy abertron@charlie"
DEFAULT_PWD='$6$60a22tNruABk1pYI$UXxwsSadDW2fBonwErvLMzmmD.MLRoEXJkjGCqXSAJDKGq4QIJLisHqVLxjTeveDGAE7VQOC8Og4i6ljYXvBL.'

apt-get update
apt-get install -y --no-install-recommends \
    sudo \
    vim

useradd -m -s /bin/bash -G sudo abertron
echo "abertron:${DEFAULT_PWD}" | chpasswd -e

touch /home/abertron/.bashrc
cat >> /home/abertron/.bashrc << EOF
export EDITOR=vim
EOF

mkdir -m 0700 /home/abertron/.ssh
echo "${DEFAULT_KEY}" > /home/abertron/.ssh/authorized_keys
chmod 0600 /home/abertron/.ssh/authorized_keys
chown -R abertron:abertron /home/abertron

userdel -r ubuntu 2>/dev/null
userdel -r debian 2>/dev/null

exit 0
