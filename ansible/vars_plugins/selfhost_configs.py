from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = """
    name: selfhost_configs
    version_added: "2.10"
    author: Aurélien Bertron <aurelienbertron@gmail.com>
    short_description: read selfhost configs
    description:
      - Loads selfhost configs into namespace 'selfhost'.
      - Configs can be defined in multiple files in a 'selfhost' subdir of standard user-specific config directory.
      - See $XDG_CONFIG_HOME on Linux.
      - For each file, variables are placed in a directory corresponding to the file name without extension.
      - By convention, file names begining by '_' are vaulted files, so these variables are merged with variables from unvaulted files.
    options:
      base_path:
        ini:
          - key: base_path
            section: vars_selfhost_configs
        env:
          - name: SELFHOST_CONFIGS_BASE_PATH
"""
import os
from ansible.errors import AnsibleParserError
from ansible.module_utils._text import to_native
from ansible.plugins.vars import BaseVarsPlugin
from ansible.utils.vars import combine_vars
from xdg import BaseDirectory

def mergedicts(dict1, dict2):
    for k in set(dict1.keys()).union(dict2.keys()):
        if k in dict1 and k in dict2:
            if isinstance(dict1[k], dict) and isinstance(dict2[k], dict):
                yield (k, dict(mergedicts(dict1[k], dict2[k])))
            else:
                # If one of the values is not a dict, you can't continue merging it.
                # Value from second dict overrides one in first and we move on.
                yield (k, dict2[k])
                # Alternatively, replace this with exception raiser to alert you of value conflicts
        elif k in dict1:
            yield (k, dict1[k])
        else:
            yield (k, dict2[k])

class VarsModule(BaseVarsPlugin):

    def get_vars(self, loader, path, entities, cache=True):

        super(VarsModule, self).get_vars(loader, path, entities)

        base_path = self.get_option("base_path")
        if not base_path:
            base_path = os.path.join(BaseDirectory.xdg_config_home, "selfhost")

        data = {"selfhost": {}}
        try:
            opath = os.path.realpath(base_path)
            self._display.debug("Looking for configs in dir '%s'" % opath)
            found_files = []

            if os.path.exists(opath) and os.path.isdir(opath):
                root_path, base_dir = os.path.split(opath)
                found_files = loader.find_vars_files(root_path, base_dir)
                if len(found_files) == 0:
                    self._display.warning("No configs found in '%s'" % opath)
            else:
                self._display.warning("Can not read configs from '%s' as it is not a directory" % opath)


            for found in found_files:
                root, ext = os.path.splitext(found)
                file_basename = os.path.basename(root)
                if (file_basename.startswith("_")):
                    file_basename = file_basename[1:]
                new_data = loader.load_from_file(found, cache=True, unsafe=True)
                if new_data:  # ignore empty files
                    data = dict(mergedicts(data, {"selfhost": {file_basename: new_data}}))

        except Exception as e:
                    raise AnsibleParserError(to_native(e))
        
        return data