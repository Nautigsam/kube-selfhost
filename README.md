# kube-selfhost

- [kube-selfhost](#kube-selfhost)
  - [Requirements](#requirements)
  - [Getting started](#getting-started)
  - [Configuration](#configuration)
  - [Services](#services)
  - [Backup](#backup)
  - [Monitoring](#monitoring)
  - [Operations](#operations)
    - [Add group in LDAP](#add-group-in-ldap)
    - [Fix LDAP memberOf property on users](#fix-ldap-memberof-property-on-users)
    - [Generate LDAP password for user](#generate-ldap-password-for-user)
  - [Reference of configs](#reference-of-configs)
    - [auth](#auth)
    - [backup](#backup-1)
    - [ldap](#ldap)
    - [mail](#mail)
    - [monitor](#monitor)
    - [nextcloud](#nextcloud)
    - [services](#services-1)
    - [traefik](#traefik)


## Requirements

- Python >= 3
- Python modules:
  - pyxdg
  - Jinja2
- Ansible >= 2.10
- Vagrant >= 2.2.6 (optional, for development environment)


## Getting started

First install ansible dependencies:
```
ansible-galaxy install -r ansible/requirements.yml
```

You will have to define all the configs used by Ansible (not automated yet). Check [Configuration](#configuration) for instructions.

Then launch development infrastructure with Vagrant:
```
vagrant up
```

In order to deploy to production, you need to create your inventory. You can draw your inspiration from `ansible/inventory.example.yml`.

Finally, deploy to production:
```
ansible-playbook -i <your inventory> -K --ask-vault-pass ansible/bootstrap_host.yml
ansible-playbook -i <your inventory> -K --ask-vault-pass ansible/docker_nodes.yml
ansible-playbook -i <your inventory> -bK --ask-vault-pass ansible/backup_nodes.yml
```


## Configuration

Before being able to deploy anything, you need to configure several variables which will be used in the services.

Configs are loaded from a base directory. You can configure this base directory by different means, which will be checked by decreasing priority:

- `SELFHOST_CONFIGS_BASE_PATH` environment variable
- `base_path` key in section `vars_selfhost_configs` of your `ansible.cfg` file
- Unix only: `$XDG_CONFIG_HOME/selfhost` which defaults to `$HOME/.config/selfhost`

If the base directory exists, Ansible will try to read every YAML file in it and load the variables in a `selfhost` namespace. Every file corresponds to a sub-namespace. If a file's name starts with an underscore `_`, it is considered as a vaulted file and its variables are merged with the variables of the corresponding non-vaulted file.

For example, if I have the following hierarchy:
```
<base dir>
├── _a.yml
├── a.yml
└── b.yml
```

Here `a.yml` and `b.yml` are non-vaulted files and `_a.yml` is a vaulted file.
```
$ cat a.yml
keyA: valueA
$ cat b.yml
keyB: valueB
$ cat _a.yml
<vaulted>
secret_keyA: valueA
</vaulted>
```

Then Ansible will load the variables like this:
```
selfhost:
  a:
    keyA: valueA
    secret_keyA: valueA
  b:
    keyB: valueB
```

This also means that for a given sub-namespace (e.g. `a` in the example), you can choose which variable will be vaulted or not.

For details about what configs need to be set, see [Reference of configs](#reference-of-configs).


## Services

Services are deployed as docker containers on the docker host.

We use Traefik reverse proxy to dispatch requests to services using domain name.
All domains are actually sub-domains of a main domain name of your choosing.

You can configure your main domain name with `traefik.domain` config.

Here is a list of the different services with associated sub-domains:

| Sub-domain  | Description                                       |
| ----------- | ------------------------------------------------- |
| auth        | Authelia, handle authentication to other services |
| ldap        | phpLDAPadmin, manage LDAP directory               |
| monitor     | Grafana visualization platform                    |
| nc          | Nextcloud productivity platform                   |
| prometheus  | Prometheus metrics database                       |
| backup-node | Node name of the backup host                      |
| docker-node | Node name of the docker host                      |


## Backup

Backup process is handled by [borgbackup](https://borgbackup.readthedocs.io/en/stable/index.html).
Since we store valuable data in docker volumes, we execute borg in a container to which we attach all the volumes to backup.

Backup host must be set for source hosts in inventory:
```yaml
docker_nodes:
  hosts:
    docker_node:
      ansible_host: my-docker-node
      backup_host: my-backup-host
```

Before beeing able to backup data, we must create a repository on the backup host:
```bash
# Here we assume your backup user is "borg" and your repository is "/backups"
$ sudo -iu borg
$ borg init /backups -e none
```

A backup script is setup in backup user's crontab to run daily.
This script stops all containers, back the volumes up and restart all containers.
Check `backup_script.j2` for details.

It is possible to view the backups on the backup host:
```bash
$ sudo -iu borg
$ borg list /backups
```


## Monitoring

Monitoring is handled with Prometheus for collecting and Grafana for visualization.

Prometheus gathers metrics on both docker and backup nodes with node-exporter.
This exporter does not handle authentication, so we use Traefik to perform basic authentication (simple username/password).

The password is specific to the host and is stored as a secret.

Be aware that node-exporter and monitoring traefik are installed as systemd services and not as containers.

Prometheus and Grafana are installed as containers on the docker node.


## Operations

### Add group in LDAP

Modifying LDAP entries is not an easy task. The `add_ldap_group` playbook simplifies the process of adding a group in LDAP.

The playbook will ask for the group name and its members:

```
$ ansible-playbook -bK ansible/add_ldap_group.yml --ask-vault-pass
BECOME password:
Vault password:
Enter a group name: group1
Enter a comma-separated list of group members: user1,user2
...
```

Note that the playbook runs on all hosts but you still need to pass it an inventory. You may limit the execution to a particular host.

If you want to execute it on the vagrant host, the full command will be:

```
ansible-playbook -bKl docker_node ansible/add_ldap_group.yml --ask-vault-pass -i .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory --extra-vars traefik_domain=local.domain --ssh-extra-args="-o StrictHostKeyChecking=no"
```

### Fix LDAP memberOf property on users

Due to an error in the ldap role, users created at container initialization do not get `memberOf` property set.
LDAP is configured to set this property according to `uniqueMember` fields in `groupOfUniqueNames` objects.

At first, the groups were created with object class `groupOfNames` and `member` fields, hence the bug.

A simple solution to fix this is to recreate groups with correct object class in phpLDAPmanager:
1. Export the group in LDIF format.
2. Delete the group.
3. Import the LDIF by changing `objectClass` to `groupOfUniqueNames` and `member` to `uniqueMember`.

### Generate LDAP password for user

OpenLDAP recommends passwords are stored as salted SHA1 (SSHA). You can use `slappasswd` tool, which is included in openldap suite.

`slappasswd` can be used interactively to allow the user to enter the password:
```
$ slappasswd -h {SSHA}
New password:
Re-enter new password:
```

Then set the `userPassword` field with the printed hash.


## Reference of configs

This section lists all the configs that are expected to be set by the user. If you don't know how to set this configs, see [Configuration](#configuration).

Configs are grouped by namespace i.e. filename. Ansible supports multiple extensions for files, so namespace `foo` can be represented either by `foo.yml` or `foo.yaml`.

### auth

| Name                   | Type   | Description                                                        |
| ---------------------- | ------ | ------------------------------------------------------------------ |
| email_password         | String | Google application password used by authelia to send notifications |
| jwt_secret             | String | The secret used by authelia to sign the JWT                        |
| session_secret         | String | The secret used by authelia to encrypt the session cookie          |
| storage_encryption_key | String | The key used by authelia to encrypt storage                        |

### backup

| Name                          | Type   | Description                                                                             |
| ----------------------------- | ------ | --------------------------------------------------------------------------------------- |
| <node_name>                   | Object | Backup configs for host <node_name> (same as in inventory), set only for backup sources |
| <node_name>.archive_prefix    | String | Backup archive prefix for this host                                                     |
| <node_name>.source_ssh_key    | String | SSH private key used to connect to backup host                                          |
| <node_name>.source_ssh_pubkey | String | SSH public key used to connect to backup host                                           |
| notification_email            | String | Email to notify when backup completes                                                   |

Example:
```yaml
# backup.yml
docker_node:
  archive_prefix: docker-node1
  source_ssh_pubkey: ssh-rsa AAA...
# _backup.yml (vaulted)
docker_node:
  source_ssh_key: |
    -----BEGIN RSA PRIVATE KEY-----
    Msdighsiogsgfg...
```

### ldap

| Name                        | Type         | Description                                                                                           |
| --------------------------- | ------------ | ----------------------------------------------------------------------------------------------------- |
| admin_pwd                   | String       | LDAP admin's password                                                                                 |
| default_groups              | List(Object) | A list of groups to create at initialization                                                          |
| default_groups[].members    | List(String) | A list of users to add to the group                                                                   |
| default_groups[].name       | String       | CN of the group                                                                                       |
| default_users               | List(Object) | A list of users to create at initialization                                                           |
| default_users[].displayName | String       | Display name of the user                                                                              |
| default_users[].email       | String       | Email address of the user                                                                             |
| default_users[].name        | String       | CN of the user                                                                                        |
| default_users[].password    | String       | Hashed password of the user (see [Generate LDAP password for user](#generate-ldap-password-for-user)) |
| readonly_user_pwd           | String       | LDAP readonly user's password                                                                         |

### mail

| Name             | Type   | Description      |
| ---------------- | ------ | ---------------- |
| password         | String | SMTP password    |
| smtp_server_host | String | SMTP server name |
| smtp_server_port | Number | SMTP server port |
| user             | String | SMTP Username    |

### monitor

| Name                   | Type   | Description                                                     |
| ---------------------- | ------ | --------------------------------------------------------------- |
| <node_name>            | Object | Monitor configs for host <node_name> (same as in inventory)     |
| <node_name>.pwd        | String | Monitoring user password, see [Monitoring section](#monitoring) |
| <node_name>.pwd_bcrypt | String | Same password, but encrypted with bcrypt                        |

In order to encrypt the password with bcrypt, you should use `htpasswd` command:
```
htpasswd -BbnC 10 "" <clear-text password> | tr -d ":"
```

Example:
```yaml
# _monitor.yml (vaulted)
backup_node:
  pwd: hgkhkhjkjlll
  pwd_bcrypt: hgkhkkhjkhjll
docker_node:
  pwd: gdhdhgfjgsds
  pwd_bcrypt: eefefefdgdgdh
```

### nextcloud

| Name                | Type   | Description                                        |
| ------------------- | ------ | -------------------------------------------------- |
| admin_pwd           | String | Nextcloud admin's password                         |
| db_pwd              | String | Nextcloud database user's password                 |
| pgsql_superuser_pwd | String | Nextcloud postgresql database superuser's password |

### services

Not all services are enabled by default. You can see it as an atempt to bring some kind of modularity.

| Name           | Type    | Description                                    |
| -------------- | ------- | ---------------------------------------------- |
| enabled        | Object  | A dictionary of booleans, one for each service |
| enabled.backup | Boolean | Defaults to `true`                             |

### traefik

| Name                          | Type   | Description                                                         |
| ----------------------------- | ------ | ------------------------------------------------------------------- |
| acme_email                    | String | The email address used to identify on ACME server                   |
| domain                        | String | The main domain name, from which all services' domains are deducted |
| secret_ovh_application_key    | String | OVH's API application key                                           |
| secret_ovh_application_secret | String | OVH's API application secret                                        |
| secret_ovh_consumer_key       | String | OVH's API consumer key                                              |
