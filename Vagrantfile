# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/bionic64"

  config.vm.define "docker_node" do |docker|
    docker.vm.network "private_network", ip: "192.168.33.10"
    docker.vm.provider "virtualbox" do |vb|
      vb.gui = false
      vb.memory = "2048"
    end
    docker.vm.provision "bootstrap", type: "ansible" do |ansible|
      ansible.compatibility_mode = "2.0"
      ansible.playbook = "ansible/bootstrap_host.yml"
      ansible.groups = {
        "docker_nodes" => ["docker_node"]
      }
      ansible.ask_vault_pass = true
      ansible.extra_vars = {
        selfhost_debug: true
      }
    end
    docker.vm.provision "services", type: "ansible" do |ansible|
      ansible.compatibility_mode = "2.0"
      ansible.playbook = "ansible/docker_nodes.yml"
      ansible.groups = {
        "docker_nodes" => ["docker_node"]
      }
      ansible.ask_vault_pass = true
      ansible.extra_vars = {
        selfhost_debug: true,
        traefik_domain: "local.domain",
        backup_host: "192.168.33.11"
      }
    end
  end

  config.vm.define "backup_node" do |backup|
    backup.vm.network "private_network", ip: "192.168.33.11"
    backup.vm.provider "virtualbox" do |vb|
      vb.gui = false
      vb.memory = "1024"
    end
    backup.vm.provision "bootstrap", type: "ansible" do |ansible|
      ansible.compatibility_mode = "2.0"
      ansible.playbook = "ansible/bootstrap_host.yml"
      ansible.groups = {
        "backup_nodes" => ["backup_node"]
      }
      ansible.ask_vault_pass = true
      ansible.become = true
      ansible.extra_vars = {
        selfhost_debug: true
      }
    end
    backup.vm.provision "services", type: "ansible" do |ansible|
      ansible.compatibility_mode = "2.0"
      ansible.playbook = "ansible/backup_nodes.yml"
      ansible.groups = {
        "backup_nodes" => ["backup_node"]
      }
      ansible.ask_vault_pass = true
      ansible.become = true
    end
  end

end
