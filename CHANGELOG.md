## 0.7.0

### BREAKING CHANGE - READ BEFORE UPGRADING

This version and the previous one (0.6.0) both contain a major upgrade to Nextcloud. Since Nextcloud only supports upgrade to the next major version, you MUST upgrade to version 0.6.0 BEFORE upgrading to the current version.

Traefik is installed as a binary to allow node monitoring. This is handled by the Ansible role [`nautigsam.traefik`](https://github.com/Nautigsam/ansible-traefik). To upgrade the binary, you MUST pass `traefik_update=yes` as an extra var when you deploy the bootstrap playbook:
```
$ ansible-playbook -i <your inventory> -K --ask-vault-pass ansible/bootstrap_host.yml --extra-vars "traefik_update=yes"
```

### Manual operations

As always with Nextcloud, some manual operations are necessary after upgrading:
```
docker exec -it -u media nextcloud_nextcloud_1 ./occ db:add-missing-indices
```

### Services

- Upgrade Jellyfin: 10.7.7 -> 10.8.5
- Upgrade Nextcloud: 21 -> 22
- Upgrade Traefik: 2.7 -> 2.9
- Upgrade Grafana: 7.5.1 -> 9.2.0
- Upgrade Prometheus: 2.25.2 -> 2.39.1
- Upgrade Authelia: 4.35.6 -> 4.36.9

## 0.6.0

### BREAKING CHANGE - READ BEFORE UPGRADING

This version and the previous one (0.5.0) both contain a major upgrade to Nextcloud. Since Nextcloud only supports upgrade to the next major version, you MUST upgrade to version 0.5.0 BEFORE upgrading to the current version.

A new config is required for Authelia, make sure to define it before upgrading:

| Name                   | Type   | Description                                 |
| ---------------------- | ------ | ------------------------------------------- |
| storage_encryption_key | String | The key used by authelia to encrypt storage |

**Note:** You can use `pwgen` tool to generate this secret. The recommended length is 128.
```
$ pwgen 128 1
```

### Manual operations

As always with Nextcloud, some manual operations are necessary after upgrading:
```
docker exec -it -u media nextcloud_nextcloud_1 ./occ db:add-missing-indices
```

### Services

- Upgrade Nextcloud: 20 -> 21
- Upgrade Jellyfin: 10.7.5 -> 10.7.7
- Upgrade Authelia: 4.28.2 -> 4.35.6
- Upgrade Traefik: 2.4 -> 2.7

## 0.5.0

### BREAKING CHANGE - READ BEFORE UPGRADING

This version and the previous one (0.4.0) both contain a major upgrade to Nextcloud. Since Nextcloud only supports upgrade to the next major version, you MUST upgrade to version 0.4.0 BEFORE upgrading to the current version.

### Manual operations

This version removes services `hybridimg` and `minecraft`. Both these services were not deployed by default. The configs `enabled.hybridimg` and `enabled.minecraft` were used to turn them on. If you enabled them, you can now remove the related configs and containers.

After the upgrade, several manual operations are necessary for Nextcloud, see version 0.4.0 notes for more details:
```
docker exec -it -u media nextcloud_nextcloud_1 ./occ db:add-missing-indices
docker exec -it -u media nextcloud_nextcloud_1 ./occ db:add-missing-columns
docker exec -it -u media nextcloud_nextcloud_1 ./occ db:convert-filecache-bigint
docker exec -it -u media nextcloud_nextcloud_1 ./occ db:add-missing-primary-keys
```

### Services

- Upgrade Nextcloud: 19 -> 20
- Remove `hybridimg` and `minecraft`
- Backup script now pauses services instead of stopping them. This made the backup process a lot more stable.

## 0.4.0

### BREAKING CHANGE - READ BEFORE UPGRADING

You must have run the upgrade to 0.3.0 BEFORE upgrading to 0.4.0. Follow upgrade instructions carefully as it can prevent you from logging in Jellyfin. There will be no more warnings about this risk in future releases.

### Manual operations

After the upgrade, Jellyfin will install most recent versions of some plugins and mark the outdated versions as "NotSupported" or "Malfunctioned". You should uninstall old versions and restart jellyfin container : `docker restart jellyfin_jellyfin_1`.

During Nextcloud upgrade, the automatic procedure skips some steps that can be time consuming. These steps can be run manually by connecting to the docker node via SSH and by running these commands:
```
docker exec -it -u media nextcloud_nextcloud_1 ./occ db:add-missing-indices
docker exec -it -u media nextcloud_nextcloud_1 ./occ db:add-missing-columns
docker exec -it -u media nextcloud_nextcloud_1 ./occ db:convert-filecache-bigint
```

### Services

- Upgrade jellyfin: 10.6.4 -> 10.7.5
- Upgrade nextcloud: 18 -> 19
- Increase the stopping grace period for some services. This prevents docker daemon to forcibly kill these containers when it tries to stop them and they do not respond quickly enough.

## v0.3.0

### BREAKING CHANGE - READ BEFORE UPGRADING

Jellyfin has been updated to 10.6.4 (last version before 10.7). This update uninstalls LDAP plugin for Jellyfin. If you use LDAP authentication, you will need to manually reinstall the plugin after the upgrade. Make sure you have at least one non-LDAP account with admin rights.

Make sure to install version **9.0.0** of the plugin, since more recent versions are compatible with Jellyfin >=10.7.0. Do not forget to restart jellyfin container: `docker restart jellyfin_jellyfin_1`

The good news is the plugin configuration is kept in memory, so the new plugin should still be configured correctly.

We plan to update jellyfin again in an upcoming version. This will finally bring us to version 10.7.x.

### Manual operations

Note that `--become` option is no more necessary when running the playbooks. The tasks needing privilege escalation are configured with `become: yes`, so that we do not need to set it playbook-wide.

### Services

- Upgrade services:
  - auth: 4.27.2 -> 4.28.2
  - jellyfin: 10.5.5 -> 10.6.4
- Enable TLS for ftp.
- Disable "Reset password" button in auth application.
- Various fixes on backup.

## v0.2.0

### Manual operations

- To upgrade traefik for monitoring, do not forget to pass `traefik_update=yes` as an extra var to `ansible-playbook` command.
  E.g.: `ansible-playbook -bK -e traefik_update=yes ansible/bootstrap_nodes.yml`
- After the upgrade, you can safely delete the file `/opt/authelia/configuration.yml` which has been moved to `/opt/authelia/conf/configuration.yml`

### Services

- Upgrade services:
  - authelia: 4.5 -> 4.27.2
  - traefik: 2.2 -> 2.4.8
  - grafana: 6.7.2 -> 7.5.1
  - prometheus: 2.17.1 -> 2.25.2
- Force borg image build before running backup.
  This prevents the backup to fail because the image is absent for some reason.
- Fix the backup logs not being included in the email notification.
- Fix default docker network being pruned, causing services restart to fail after backup.

## v0.1.0

### Misc

- Create CHANGELOG

### Services

- Rework of backup:
  - Use systemd timer instead of cron
  - Use ssmtp to send recap email after backup
  - In case of backup error, backup script now returns with same error code as borg process

### Configs

- Add `backup.notification_email`
- Add `mail.smtp_server_host` (previously hard-coded in Nextcloud configuration)
- Add `mail.smtp_server_port` (previously hard-coded in Nextcloud configuration)
- Add `services.enabled.backup` (defaults to `true`)
- Move `nextcloud.mail_pwd` to `mail.password`
- Move `nextcloud.mail_user` to `mail.user`
